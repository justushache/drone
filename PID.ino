  PID::PID(double _Kp,double _Ki,double _Kd){
    Kp=_Kp;
    Ki=_Ki;
    Kd=_Kd;
  }
  
  int PID::calculate(double setValue,double realValue){
    
    double PIDi;
    double error=realValue-setValue;
    
    if(lastError==13371337)
      lastError=error;
    if(!(-3>error>3))
    integral+=error;
    
    PIDi=Kp*(error);
    PIDi+=integral*Ki; 
    PIDi+=Kd*(error-lastError);
    lastError=error;
  
    return (int)PIDi;
  }
  
  void PID::changeGains(double pOffset,double iOffset,double dOffset){ 
    Kp=Kp+pOffset;
    Ki=Ki+iOffset;
    Kd=Kd+dOffset;
    }
  
    double PID::getP(){
      return Kp;
      }
    double PID::getI(){
      return Ki;      
      }
    double PID::getD(){
      return Kd;      
      }
