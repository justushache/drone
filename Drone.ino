#include <EEPROM.h>  
#include "Drone.h"

int maxAngle=20;
int itime;
bool lastSW1;
bool on;

bool setupBool = true;

int icounter;

data_s data;
data_s lastdata;
gyroData_s  gyroData;

double initP=0.0;
double initI=0.0;
double initD=45.0;

int P1;
int P2;
int P3;
int maxCorrection=300;

PID PID1(initP,initI,initD); 
PID PID2(initP,initI,initD); 
PID PID3(5,0,0);

bool calib = 0;
bool bStart =false;
int calibVal =0;
double tempSetYaw;

int nrfFailCounter = 0;

double tmpR,tmpP,counter;

double setYaw=0;

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  
  pinMode(PC13,OUTPUT);
  
  setupNRF(); 
  checkNRF();

  if(data.ly>10)
    while(true);
   
  gyroSetup();
  delay(500);
  for(int a=0;a<1000;a++){
  droneLoop();}
  bStart=true;
  
  setYaw=gyroData.Yaw;
  delay(1000);
   
  setYaw=gyroData.Yaw;
  escSetup();
  
  if(calib==1){
    Serial.println("HIGH");
  calibVal = 2000;
    escWrite(1,calibVal);
    escWrite(2,calibVal);
    escWrite(3,calibVal);
    escWrite(4,calibVal);
  delay (10000);
    Serial.println("LOW");
  calibVal = 1000;
    escWrite(1,calibVal);
    escWrite(2,calibVal);
    escWrite(3,calibVal);
    escWrite(4,calibVal);
  delay (10000);
  }
  
}

void loop() {
  tmpR+=gyroData.Roll;
  tmpP+=gyroData.Pitch;
  counter++;

  Serial.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  Serial.print("Time active:");                   Serial.println(itime);


  Serial.print("Tilt: Roll:");                    Serial.println(gyroData.Roll);
  if(gyroData.Roll<0)Serial.print("left");else Serial.print("right");
  Serial.print(" Pitch:");                        Serial.println(gyroData.Pitch);
  if(gyroData.Pitch<0)Serial.println("front down");else Serial.println("front up");

  Serial.print("Yaw angle:");                     Serial.print(gyroData.Yaw);
  if( 5<gyroData.Roll-gyroData.lastRoll   ||-5>gyroData.Roll-gyroData.lastRoll||
      5<gyroData.Pitch-gyroData.lastPitch ||-5>gyroData.Pitch-gyroData.lastPitch||
      5<gyroData.Yaw-gyroData.lastYaw     ||-5>gyroData.Yaw-gyroData.lastYaw   )Serial.println("HGIHIHI");
      
  Serial.println("transmitter joysticks");
  Serial.print("right x-Direction");              Serial.println(data.rx);
  Serial.print("right y-Direction");              Serial.println(data.ry);
  Serial.print("left  x-Direction");              Serial.print(data.lx);
  Serial.print("left  y-Direction");              Serial.println(data.ly);
   
  Serial.println("transmitter switches:");
    Serial.print(data.sw1);
    Serial.print(data.sw2);
    Serial.print(data.sw3);
    Serial.print(data.sw4);
    Serial.print(data.sw5);
    Serial.print(data.sw6);
    Serial.print(data.sw7);
    Serial.print(data.sw8);
    Serial.print(data.sw9);
    Serial.println(data.sw10);
  Serial.print("set yaw:");                         Serial.println(setYaw);
  Serial.print("temp set yaw:");                    Serial.print(tempSetYaw);
    
 Serial.print("PID - roll  corretion:");          Serial.print(P1);
 Serial.print("PID - pitch corretion:");          Serial.println(P2);
 Serial.print("PID - yaw   corretion:");          Serial.println(P3);
    uint16_t P;
    EEPROM.read(0x10,&P);
    
Serial.print("last saved calibration Value");     Serial.println(P/100.0);

Serial.print("PID - P:");                         Serial.println(PID1.getP());
Serial.print("PID - P:");                         Serial.println(PID1.getI());
Serial.print("PID - P:");                         Serial.println(PID1.getD());

delay(10);
 
}

double correctSet(double real, double set){
  if(set>180)   set=-180;
  if(set<-180)  set=180;  
  
  if(90 >real && real>0 &&set<-160){
    set=real+90;
    return set;
  }
  
  if(-90<real && real<0 &&set>160 ){
    set=real-90;
    return set;
  }

  if( (90>real  && real>-90)  ||  (-90>=real&& set<=5)  ||  (90<=real && set>=-5)){
        if(set<real-90)set=real-90;
        if(set>real+90)set=real+90;
        return set;       
  }
  
  if(real>=85&& set<=-85){
    if(set+360-real>90){set=real-270;
        return set;}
  }
  
  if(real<=-85&&set>=85){
    if(set-360-real<-90){set=real+270;
        return set;}
  }
  return set;
  }

void droneLoop(){
  if(calib==1)return;
  gyroCalculate();
  
  if(checkNRF()){
    nrfFailCounter=0;}
  else{
    nrfFailCounter++;}
   digitalWrite(PC13,HIGH);

  if((!(475<data.lx&&data.lx<525))&&data.rx!=0&&data.lx!=0&&data.ry!=0&&data.ly!=0){
  setYaw-=((data.lx-500.0)/500.0);
  }
  setYaw=correctSet(gyroData.Yaw,setYaw);
  
  if(setupBool==true){
    if(data.sw9==true&&lastdata.sw9==false&&PID1.getP()>0.09){
      PID1.changeGains(-0.1,0,0);
      PID2.changeGains(-0.1,0,0);
      }
    if(data.sw10==true&&lastdata.sw10==false){
      PID1.changeGains(+0.1,0,0);
      PID2.changeGains(+0.1,0,0);
      }    
        
    if((data.sw1==false&&lastdata.sw1==true)||(data.sw6==false&&lastdata.sw6==true)){
      EEPROM.update(0x10,(uint16_t)(PID1.getP()*100));
      }
    }


tempSetYaw=setYaw;
    if(setYaw>gyroData.Yaw){
          if(setYaw-gyroData.Yaw>120)
            tempSetYaw-=360;
      }else{
          if(setYaw-gyroData.Yaw<-120)
            tempSetYaw+=360;       
        }

  if(data.ly < 25 || nrfFailCounter>15){
    digitalWrite(PC13,LOW);
  escWrite(1,500);
  escWrite(2,500);
  escWrite(3,500);
  escWrite(4,500);
  on=false;
  lastdata=data;
  setYaw=gyroData.Yaw;
  return;
  }



  
  on=true;
  P1 = PID1.calculate(gyroData.Roll,  ((double)data.rx-500)/500*maxAngle);
  P2 = PID2.calculate(gyroData.Pitch, ((double)data.ry-500)/500*maxAngle);
  P3 = PID3.calculate(gyroData.Yaw,   tempSetYaw);
  
  if(P1>maxCorrection)
    P1=maxCorrection;
  if(P1<-maxCorrection)
    P1=-maxCorrection;
    
  if(P2>maxCorrection)
    P2=maxCorrection;
  if(P2<-maxCorrection)
    P2=-maxCorrection;
    
  if(P3>maxCorrection/2)
    P3=maxCorrection/2;
  if(P3<-maxCorrection/2)
    P3=-maxCorrection/2;    
    
    int out1=1000+data.ly+P2+P1+P3 ;
    int out2=1000+data.ly+P2-P1-P3;
    int out3=1000+data.ly-P2-P1+P3;
    int out4=1000+data.ly-P2+P1-P3;

    if(out1<1000)out1=1000;
    if(out1>2000)out1=2000;
    if(out2<1000)out1=1000;
    if(out2>2000)out1=2000;
    if(out3<1000)out1=1000;
    if(out3>2000)out1=2000;
    if(out4<1000)out1=1000;
    if(out4>2000)out1=2000;
    
    escWrite(1,out1);
    escWrite(2,out2);
    escWrite(3,out3);
    escWrite(4,out4);

    
  if(calib==1){
    Serial.println("calibrating");
    Serial.println(calibVal);
    escWrite(1,calibVal);
    escWrite(2,calibVal);
    escWrite(3,calibVal);
    escWrite(4,calibVal);
    return;    
    }

    
    lastdata=data;
}
