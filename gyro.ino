
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"

#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

MPU6050 mpu;

#define INTERRUPT_PIN PB5  // use pin 2 on Arduino Uno & most boards
#define LED_PIN PA13 // (Arduino is 13, Teensy is 11, Teensy++ is 6)

bool first=true;
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector



volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}



 
void gyroSetup(){ 
  
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        Wire.setClock(400000); // 400kHz I2C clock. Comment this line if having compilation difficulties
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    mpu.initialize();
    pinMode(INTERRUPT_PIN, INPUT);

    devStatus = mpu.dmpInitialize();

    mpu.setXGyroOffset(150);
    mpu.setYGyroOffset(12);
    mpu.setZGyroOffset(-37);
    mpu.setZAccelOffset(791); // 1688 factory default for my test chip

    mpu.setDMPEnabled(true);

    digitalPinToInterrupt(INTERRUPT_PIN);
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    dmpReady = true;
    packetSize = mpu.dmpGetFIFOPacketSize();
    mpu.setRate(0);
    delay(500);
    first=true;
  }

void gyroCalculate(){
  
    if (!dmpReady) return;
    while (!mpuInterrupt && fifoCount < packetSize) {
        if (mpuInterrupt && fifoCount < packetSize) {
          // try to get out of the infinite loop 
          fifoCount = mpu.getFIFOCount();
        }  
    }

    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    fifoCount = mpu.getFIFOCount();

    if ((mpuIntStatus & _BV(MPU6050_INTERRUPT_FIFO_OFLOW_BIT)) || fifoCount >= 1024) {
        mpu.resetFIFO();
        fifoCount = mpu.getFIFOCount();
        Serial.println(F("FIFO overflow!"));

    } else if (mpuIntStatus & _BV(MPU6050_INTERRUPT_DMP_INT_BIT)) {
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        fifoCount -= packetSize;

    mpu.dmpGetQuaternion(&q, fifoBuffer);
    mpu.dmpGetGravity(&gravity, &q);
    mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
    
    gyroData.lastYaw  =gyroData.Yaw; 
    gyroData.lastPitch=gyroData.Pitch;
    gyroData.lastRoll =gyroData.Roll;
    
    gyroData.Yaw=ypr[0]   * 180/M_PI;
    gyroData.Pitch=ypr[1] * 180/M_PI-8.92;
    gyroData.Roll=ypr[2]  * 180/M_PI-2;

    
  if(gyroData.lastRoll!=0.0&&bStart==true&&(10<gyroData.Roll-gyroData.lastRoll   ||-10>gyroData.Roll-gyroData.lastRoll||
      10<gyroData.Pitch-gyroData.lastPitch ||-10>gyroData.Pitch-gyroData.lastPitch||
      ((10<gyroData.Yaw-gyroData.lastYaw ||-10>gyroData.Yaw-gyroData.lastYaw)&&!(gyroData.lastYaw<-170||gyroData.lastYaw>170))))
      {        
    gyroData.Yaw  =gyroData.lastYaw  ; 
    gyroData.Pitch=gyroData.lastPitch;
    gyroData.Roll =gyroData.lastRoll ;
        
        }
    
        mpu.resetFIFO();
    
    
  }
}

  
