#include <SPI.h>
#include <RF24_STM32.h>
#include <nRF24L01_STM32.h>

RF24 radio (PB1,PB0 );// CE,CSN,IRQ
const uint64_t rxaddr = 133713371337;



int calculateChecksum(){
  return data.lx+
  data.ly+
  data.rx+
  data.ry;
  }

void setupNRF(){
  SPI.begin(); 
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);

  radio.begin();
  radio.openReadingPipe(1,rxaddr);
  radio.startListening();
  }
  
bool checkNRF(){
  if(!radio.available())return false;
  radio.read(&data,sizeof(data));
  return true;
 }
