//gyro functions
void gyroSetup();
void gyroCalculate();
void gyroCalibrate();

//esc functions
void escSetup();
void escWrite(int escNumber,int escValue);

//NRF24 functions
int calculateChecksum();
void setupNRF();
bool checkNRF();

//PID class
class PID{
  private:
    double Kp;
    double Ki;
    double Kd;
    double lastError=13371337;
    double integral=0;

  public:
    PID(double _Kp,double _Ki,double _Kd);
    int calculate(double setValue,double realValue);
    void changeGains(double pOffset,double iOffset,double dOffset);
    double getP();
    double getI();
    double getD();
};

struct gyroData_s{
   double Roll, Pitch, Yaw;                    //Calculatet Values in Degrees
   double lastRoll,lastPitch,lastYaw;
  };

struct data_s{                                //MUST NOT BE BIGGER THAN 32 BYTES
  
  uint16_t lx,ly,rx,ry;                       //values of the joysticks in one-tenth of a percent  
  bool sw1,sw2,sw3,sw4,sw5,sw6,sw7,sw8,sw9,sw10;               //values of the switches
  
  uint32_t checkSum;                          //checksum to validate sended data
};
