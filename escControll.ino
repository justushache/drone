void escSetup(){
  pinMode(PA0,PWM);
  pinMode(PA1,PWM);
  pinMode(PA2,PWM);
  pinMode(PA3,PWM);
  TIMER2_BASE->CR1=   TIMER_CR1_CEN|TIMER_CR1_ARPE;
  TIMER2_BASE->CR2=   0;
  TIMER2_BASE->SMCR=  0;
  TIMER2_BASE->DIER=  0;
  TIMER2_BASE->EGR=   0;
  TIMER2_BASE->CCMR1 = (0b110 << 4) | TIMER_CCMR1_OC1PE | (0b110 << 12) | TIMER_CCMR1_OC2PE;
  TIMER2_BASE->CCMR2 = (0b110 << 4) | TIMER_CCMR2_OC3PE | (0b110 << 12) | TIMER_CCMR2_OC4PE;
  TIMER2_BASE->CNT=   5000;
  TIMER2_BASE->CCER = TIMER_CCER_CC1E|TIMER_CCER_CC2E|TIMER_CCER_CC3E|TIMER_CCER_CC4E;
  TIMER2_BASE->PSC=   71;
  TIMER2_BASE->ARR=   10000;
  TIMER2_BASE->DCR=   0;
  TIMER2_BASE->CCR1=  1000;
  TIMER2_BASE->CCR2=  1000;
  TIMER2_BASE->CCR3=  1000;
  TIMER2_BASE->CCR4=  1000;
  attachInterrupt(PA0, droneLoop, FALLING);
  }

void escWrite(int escNumber,int escValue){
      switch(escNumber){
    case 1:
      TIMER2_BASE->CCR1=escValue;
      break;
    case 2:
      TIMER2_BASE->CCR2=escValue;
      break;
    case 3:
      TIMER2_BASE->CCR3=escValue;
      break;
    case 4:
      TIMER2_BASE->CCR4=escValue;
      break;
      }
   }
